import qrcode
import cv2

# Fungsi untuk menghasilkan QR Code
def generate_qr_code(data, filename):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    img.save(filename)

# Fungsi untuk menampilkan gambar dalam mode fullscreen
def show_fullscreen_image(image_path):
    # Membaca gambar menggunakan OpenCV
    image = cv2.imread(image_path)

    # Mendapatkan resolusi layar
    screen_width = 0
    screen_height = 0
    cv2.namedWindow("fullscreen", cv2.WND_PROP_FULLSCREEN)
    if cv2.getWindowProperty("fullscreen", cv2.WND_PROP_FULLSCREEN) != -1:
        screen_width = cv2.getSystemWindowProperty("fullscreen", cv2.WND_PROP_WIDTH)
        screen_height = cv2.getSystemWindowProperty("fullscreen", cv2.WND_PROP_HEIGHT)

    # Menampilkan gambar dalam mode fullscreen
    cv2.namedWindow("fullscreen", cv2.WINDOW_NORMAL)
    cv2.setWindowProperty("fullscreen", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    cv2.imshow("fullscreen", image)

    # Menunggu tombol 'q' ditekan untuk keluar
    while True:
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

    # Membersihkan dan menutup jendela
    cv2.destroyAllWindows()

# Menerima input dari pengguna
input_data = input("Masukkan data untuk QR Code: ")

# Menghasilkan QR Code
filename = "/home/pi/rpi-parkir/qr.jpg"
generate_qr_code(input_data, filename)

# Menampilkan gambar QR Code dalam mode fullscreen
show_fullscreen_image(filename)
