import cv2
import numpy as np

# membaca gambar
img = cv2.imread('/home/pi/rpi-parkir/foto.jpg')

# mengubah gambar ke dalam grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# edge detection dengan Canny edge detector
edges = cv2.Canny(gray, 50, 150, apertureSize=3)

# proses kontur untuk mendeteksi persegi panjang pada gambar
contours, hierarchy = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

for cnt in contours:
    approx = cv2.approxPolyDP(cnt, 0.01*cv2.arcLength(cnt,True),True)
    if len(approx)==4:
        cv2.drawContours(img,[approx],0,(0,255,0),2)

# menampilkan gambar yang sudah dipotong
cv2.imshow('Persegi Panjang', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
