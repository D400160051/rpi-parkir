import cv2

# Inisialisasi kamera
camera = cv2.VideoCapture(0)

# Ambil foto
return_value, image = camera.read()

# Simpan foto pada direktori tertentu
cv2.imwrite('/home/pi/rpi-parkir/foto.jpg', image)

# Bebaskan kamera
del(camera)
