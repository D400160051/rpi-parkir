import cv2
import pytesseract
# Inisialisasi kamera
camera = cv2.VideoCapture(0)

# Ambil foto
print("ambil foto")
return_value, image = camera.read()

# Simpan foto pada direktori tertentu
cv2.imwrite('/home/pi/rpi-parkir/foto.jpg', image)
print("simpan foto")
# Bebaskan kamera
del(camera)

# Load gambar
img = cv2.imread('/home/pi/rpi-parkir/foto.jpg')
print("load foto")
# Konversi ke grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
print("grayscale")
blur = cv2.bilateralFilter(gray, 11,90, 90)
print("blur")
# Preprocessing gambar dengan thresholding
thresh = cv2.threshold(blur, 0, 200, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
print("treshold")

# Konfigurasi Tesseract OCR engine
custom_config = r'--oem 3 --psm 6 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

# Proses OCR pada ROI
plat_nomor = pytesseract.image_to_string(thresh, config=custom_config)
print("cek baca")
# Tampilkan hasil
print('Plat nomor kendaraan:', plat_nomor)

# tampilkan gambar hasil filtering
cv2.imshow('Hasil Filtering',thresh)

cv2.waitKey(0)
cv2.destroyAllWindows()