import cv2

# baca gambar
img = cv2.imread('/home/pi/rpi-parkir/foto.jpg')

# konversi gambar ke grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# deteksi tepi pada gambar menggunakan operator Canny
edged = cv2.Canny(gray, 30, 200)

# proses pencarian kontur pada gambar hasil deteksi tepi
contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Lakukan pengurutan kontur secara menurun berdasarkan ukuran luas area
contours = sorted(contours, key=cv2.contourArea, reverse=True)

# Lakukan iterasi pada setiap kontur dan cek apakah bentuknya mirip dengan rectangle
for contour in contours:
    # cari approximasi polygon dari kontur
    approx = cv2.approxPolyDP(contour, 0.01*cv2.arcLength(contour, True), True)

    # jika terdapat 4 titik pada polygon, maka bentuknya mirip dengan rectangle
    if len(approx) == 4:
        # gambar rectangle pada gambar
        cv2.drawContours(img, [approx], 0, (0, 255, 0), 2)
        break

# tampilkan gambar hasil deteksi
cv2.imshow("Hasil Deteksi Rectangle", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
