import cv2

# Buka kamera dengan OpenCV
cap = cv2.VideoCapture(0)
# Mengatur resolusi output
width = 640
height = 480
cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
# Ulangi frame dari kamera
while True:
     ret, frame = cap.read()

    # Memperkecil ukuran frame
    resized_frame = cv2.resize(frame, (width//2, height//2))

    # Menampilkan frame yang telah diperkecil
    cv2.imshow('Resized Frame', resized_frame)

    # Tekan tombol q untuk keluar
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Bebaskan sumber daya
cap.release()
cv2.destroyAllWindows()
