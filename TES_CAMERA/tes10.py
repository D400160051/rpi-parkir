import cv2

def crop_license_plate(image_path):
    # Load gambar plat nomor
    image = cv2.imread(image_path)

    # Definisikan koordinat bounding box untuk kode wilayah dan nomor
    x_start, y_start, x_end, y_end = 100, 100, 400, 200

    # Crop bagian kode wilayah dari gambar
    region_code = image[y_start:y_end, x_start:x_end]

    # Update koordinat bounding box untuk nomor
    x_start, y_start, x_end, y_end = 500, 100, 900, 200

    # Crop bagian nomor dari gambar
    region_number = image[y_start:y_end, x_start:x_end]

    return region_code, region_number

# Contoh penggunaan
image_path = '/home/pi/rpi-parkir/img.jpeg'
region_code, region_number = crop_license_plate(image_path)

# Simpan hasil crop ke file gambar
cv2.imwrite('/home/pi/rpi-parkir/region_code.jpg', region_code)
cv2.imwrite('/home/pi/rpi-parkir/region_number.jpg', region_number)
