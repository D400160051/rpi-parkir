import cv2
import numpy as np
import pytesseract
camera = cv2.VideoCapture(0)

# Ambil foto
print("ambil foto")
return_value, image = camera.read()

# Simpan foto pada direktori tertentu
cv2.imwrite('/home/pi/rpi-parkir/foto.jpg', image)
print("simpan foto")
# Bebaskan kamera
del(camera)

# Load gambar
img = cv2.imread('/home/pi/rpi-parkir/foto.jpg')
#img = cv2.imread('/home/pi/rpi-parkir/1.jpg')
print("load foto")
# Konversi ke grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
print("grayscale")

blur = cv2.bilateralFilter(gray, 11,90, 90)
print("blur")

thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
print("treshold")

edges = cv2.Canny(blur, 30, 200)
print("edges")

# Temukan kontur pada gambar
contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
# Pilih kontur dengan luas terbesar (diasumsikan sebagai plat nomor)
largest_contour = max(contours, key=cv2.contourArea)
    
# Hitung bounding rectangle dari kontur terpilih
x, y, w, h = cv2.boundingRect(largest_contour)
    
# Gambar persegi panjang pada plat nomor
cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
    
# Potong gambar sesuai dengan bounding rectangle
cropped_image = img[y:y+h, x:x+w]

gray1 = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2GRAY)
print("grayscale")
# Preprocessing gambar dengan thresholding
thresh1 = cv2.threshold(gray1, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
print("treshold")
# Batasi region-of-interest (ROI) pada gambar
x, y, w, h = 400, 200, 500, 200
roi = cropped_image[y:y+h, x:x+w]

# Konfigurasi Tesseract OCR engine
custom_config = r'--oem 3 --psm 6 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

# Proses OCR pada ROI
plat_nomor = pytesseract.image_to_string(thresh1, config=custom_config)
print("cek baca")   
# Tampilkan hasil
print('Plat nomor kendaraan:', plat_nomor)    
# Tampilkan gambar hasil

#cv2.imshow('1', edges)
cv2.imshow('Cropped License Plate', cropped_image)
cv2.waitKey(0)
cv2.destroyAllWindows()