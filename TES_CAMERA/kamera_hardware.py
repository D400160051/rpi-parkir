import cv2

def check_camera_detection(hw):
    cap = cv2.VideoCapture(hw)
    if cap.isOpened():
        print("Kamera terdeteksi")
        cap.release()
    else:
        print("Kamera tidak terdeteksi")        

check_camera_detection(0)
check_camera_detection(2)