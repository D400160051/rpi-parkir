import cv2

# Membuka kamera pertama
cap1 = cv2.VideoCapture(0)
# Membuka kamera kedua
cap2 = cv2.VideoCapture(2)

# Mengatur resolusi output kamera pertama
width1 = 640
height1 = 480
cap1.set(cv2.CAP_PROP_FRAME_WIDTH, width1)
cap1.set(cv2.CAP_PROP_FRAME_HEIGHT, height1)

# Mengatur resolusi output kamera kedua
width2 = 640
height2 = 480
cap2.set(cv2.CAP_PROP_FRAME_WIDTH, width2)
cap2.set(cv2.CAP_PROP_FRAME_HEIGHT, height2)

while True:
    # Membaca frame dari kamera pertama
    ret1, frame1 = cap1.read()

    # Memperkecil ukuran frame pertama
    resized_frame1 = cv2.resize(frame1, (width1 // 2, height1 // 2))

    # Menampilkan frame pertama yang telah diperkecil
    cv2.imshow('Resized Frame 1', resized_frame1)

    # Membaca frame dari kamera kedua
    ret2, frame2 = cap2.read()

    # Memperkecil ukuran frame kedua
    resized_frame2 = cv2.resize(frame2, (width2 // 2, height2 // 2))

    # Menampilkan frame kedua yang telah diperkecil
    cv2.imshow('Resized Frame 2', resized_frame2)

    # Menekan tombol 'q' untuk keluar dari loop
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Melepaskan kamera dan menutup jendela
cap1.release()
cap2.release()
cv2.destroyAllWindows()
