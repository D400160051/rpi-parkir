
import pyrebase

# konfigurasi
config = {
  "apiKey": "AIzaSyDRSpkJbYDkAwdS0mA9kugA0W9dl28LtzI",
  "authDomain": "parkir-rpi.firebaseapp.com",
  "databaseURL": "https://parkir-rpi-default-rtdb.asia-southeast1.firebasedatabase.app",
  "projectId": "parkir-rpi",
  "storageBucket": "parkir-rpi.appspot.com",
  "messagingSenderId": "976859551827",
  "appId": "1:976859551827:web:038419c74329397d7b4ea6",
  "measurementId": "G-9VMNCN62KL"
};

# Inisialisasi Firebase
firebase = pyrebase.initialize_app(config)
db = firebase.database()

# Menulis data ke Firebase Realtime Database
data = {"nama": "tesi", "usia": 33}
db.child("pengguna").child("2").set(data)

# Membaca data dari Firebase Realtime Database
data = db.child("pengguna").child("2").get().val()
print(data)

