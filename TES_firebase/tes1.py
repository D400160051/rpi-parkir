import pyrebase

config = {
  "apiKey": "AIzaSyDRSpkJbYDkAwdS0mA9kugA0W9dl28LtzI",
  "authDomain": "parkir-rpi.firebaseapp.com",
  "databaseURL": "https://parkir-rpi-default-rtdb.asia-southeast1.firebasedatabase.app",
  "projectId": "parkir-rpi",
  "storageBucket": "parkir-rpi.appspot.com",
  "messagingSenderId": "976859551827",
  "appId": "1:976859551827:web:038419c74329397d7b4ea6",
  "measurementId": "G-9VMNCN62KL"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()

# Definisikan fungsi trigger
def get_data_from_firebase(data):
    print(data["path"])
    print(data["data"])

# Aktifkan trigger untuk path tertentu
db.child("pengguna").stream(get_data_from_firebase)
