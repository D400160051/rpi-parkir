import pyrebase
import time

config = {
    "apiKey": "AIzaSyDRSpkJbYDkAwdS0mA9kugA0W9dl28LtzI",
    "authDomain": "parkir-rpi.firebaseapp.com",
    "databaseURL": "https://parkir-rpi-default-rtdb.asia-southeast1.firebasedatabase.app",
    "storageBucket": "parkir-rpi.appspot.com"
}
firebase = pyrebase.initialize_app(config)
db = firebase.database()

while True:
    current_time = int(time.time())  # waktu saat ini dalam detik
    if current_time % 5 == 0:  # jika kelipatan 5 detik
        print("Waktu saat ini: " + str(current_time))
        data = db.child("cek").child("0").get()
        print("Name: " + str(data.val()))
    time.sleep(1)  # tunggu 1 detik sebelum mengecek waktu lagi
    print("Mengecek waktu lagi...")
