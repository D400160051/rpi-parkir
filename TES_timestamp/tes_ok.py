import datetime
import time
import os


def get_cpu_temperature():
    # Baca suhu CPU dari file sistem thermal_zone0
    with open('/sys/class/thermal/thermal_zone0/temp', 'r') as f:
        temp = float(f.read()) / 1000.0
        
    return temp



while True:
    # Ambil waktu saat ini
    now = datetime.datetime.now()
    
    # Tampilkan waktu dengan format yang diinginkan
    print('JAM', now.strftime('%Y-%m-%d %H:%M:%S'))
     # Ambil suhu CPU saat ini
    cpu_temp = get_cpu_temperature()
    
    # Tampilkan suhu CPU
    print('Suhu CPU: {:.2f} '.format(cpu_temp))
    print('')
    
    # Tunggu satu detik sebelum menampilkan waktu berikutnya
    time.sleep(1)
