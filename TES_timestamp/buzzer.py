import RPi.GPIO as GPIO
import time

# Set pin numbering mode
GPIO.setmode(GPIO.BCM)

# Set pin 21 as output
GPIO.setup(21, GPIO.OUT)

def beep(duration):
    GPIO.output(21, GPIO.HIGH)  # Turn on beep
    time.sleep(duration)        # Wait for the specified duration
    GPIO.output(21, GPIO.LOW)   # Turn off beep

# Contoh penggunaan: Beep selama 1 detik
beep(1)

# Reset GPIO settings
GPIO.cleanup()
