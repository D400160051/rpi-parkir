import pyrebase

class PyrebaseDatabase:
    def __init__(self, config):
        self.firebase = pyrebase.initialize_app(config)
        self.db = self.firebase.database()

    def add_data(self, database_name, node_path, data):
        self.db.child(database_name).child(node_path).push(data)

    def update_data(self, database_name, node_path, data):
        self.db.child(database_name).child(node_path).update(data)

    def delete_data(self, database_name, node_path):
        self.db.child(database_name).child(node_path).remove()

    def read_data(self, database_name, node_path):
        return self.db.child(database_name).child(node_path).get().val()

    def close_connection(self):
        # Tidak ada aksi penutupan koneksi yang diperlukan pada Pyrebase
        pass
