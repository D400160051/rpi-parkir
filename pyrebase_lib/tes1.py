import re

def deteksi_plat_nomor(plat_nomor):
    pola = r"[A-Z]{1,3}\d{4}[A-Z]{2,3}$"
    hasil = re.match(pola, plat_nomor)
    if hasil:
        return True
    else:
        return False

# Contoh penggunaan
plat_nomor1 = "B4756AZ"
plat_nomor2 = "AB1234CD"
plat_nomor3 = "A123BC"

if deteksi_plat_nomor(plat_nomor1):
    print(plat_nomor1, "adalah plat nomor Indonesia yang valid.")
else:
    print(plat_nomor1, "bukan plat nomor Indonesia yang valid.")

if deteksi_plat_nomor(plat_nomor2):
    print(plat_nomor2, "adalah plat nomor Indonesia yang valid.")
else:
    print(plat_nomor2, "bukan plat nomor Indonesia yang valid.")

if deteksi_plat_nomor(plat_nomor3):
    print(plat_nomor3, "adalah plat nomor Indonesia yang valid.")
else:
    print(plat_nomor3, "bukan plat nomor Indonesia yang valid.")
