# main.py

from pyrebase_database import PyrebaseDatabase

# Konfigurasi Pyrebase
config = {
    "apiKey": "AIzaSyDRSpkJbYDkAwdS0mA9kugA0W9dl28LtzI",
    "authDomain": "parkir-rpi.firebaseapp.com",
    "databaseURL": "https://parkir-rpi-default-rtdb.asia-southeast1.firebasedatabase.app",
    "storageBucket": "parkir-rpi.appspot.com"
}

# Inisialisasi objek PyrebaseDatabase
pyrebase_db = PyrebaseDatabase(config)

# Mengisi data plat nomor dan jam masuk
plat_nomor = "B 1234 AB"
jam_masuk = "09:00"
pyrebase_db.add_data("parkir", "car", {"plat_nomor": plat_nomor, "jam_masuk": jam_masuk})

# Mencari data sesuai plat nomor
data = pyrebase_db.read_data("parkir", "car", {"plat_nomor": plat_nomor})
if data:
    print("Data ditemukan:")
    print(data)
else:
    print("Data tidak ditemukan")

# Mengupdate jam keluar sesuai plat nomor
jam_keluar = "18:00"
if data:
    data_key = list(data.keys())[0]
    pyrebase_db.update_data("parkir", f"car/{data_key}", {"jam_keluar": jam_keluar})
    print("Jam keluar berhasil diupdate")
else:
    print("Data tidak ditemukan, tidak dapat melakukan update")

# Menampilkan data setelah diupdate
updated_data = pyrebase_db.read_data("parkir", "car", {"plat_nomor": plat_nomor})
if updated_data:
    print("Data setelah diupdate:")
    print(updated_data)
else:
    print("Data tidak ditemukan setelah diupdate")

# Menutup koneksi
pyrebase_db.close_connection()