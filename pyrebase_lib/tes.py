
import pyrebase
from datetime import datetime

# Konfigurasi Pyrebase
config = {
    "apiKey": "AIzaSyDRSpkJbYDkAwdS0mA9kugA0W9dl28LtzI",
    "authDomain": "parkir-rpi.firebaseapp.com",
    "databaseURL": "https://parkir-rpi-default-rtdb.asia-southeast1.firebasedatabase.app",
    "storageBucket": "parkir-rpi.appspot.com"
}

# Inisialisasi Firebase
firebase = pyrebase.initialize_app(config)
database = firebase.database()

# Fungsi untuk menambahkan data parkir/member
def tambah_data_parkir(plat_nomor, member=False):
    jam_masuk = datetime.now().strftime("%H:%M")  # Mengambil jam masuk saat ini
    tanggal = datetime.now().strftime("%Y-%m-%d")  # Mengambil tanggal saat ini
    data = {
        "plat_nomor": plat_nomor,
        "jam_masuk": jam_masuk,
        "jam_keluar": "",  # Nilai awal jam keluar sebagai None
        "tanggal": tanggal,  # Menambahkan data tanggal
        "member": member  # Menambahkan data member
    }
    database.child("parkir").child(plat_nomor).set(data)

# Fungsi untuk mengisi jam keluar
def isi_jam_keluar(plat_nomor, jam_keluar):
    parkir = database.child("parkir").get()
    for entry in parkir.each():
        data = entry.val()
        if data["plat_nomor"] == plat_nomor:
            database.child("parkir").child(entry.key()).update({"jam_keluar": jam_keluar})
            print("Jam keluar telah diupdate.")
            return
    print("Data parkir tidak ditemukan.")

# Fungsi untuk mencari dan menampilkan data parkir
def cari_data_parkir(plat_nomor):
    parkir = database.child("parkir").get()
    for entry in parkir.each():
        data = entry.val()
        if data["plat_nomor"] == plat_nomor:
            print("Data parkir ditemukan:")
            print("Plat Nomor:", data["plat_nomor"])
            print("Jam Masuk:", data["jam_masuk"])
            print("Jam Keluar:", data["jam_keluar"])
            print("Tanggal:", data["tanggal"])  # Menampilkan data tanggal
            print("Member:", data["member"])  # Menampilkan data member
            return
    print("Data parkir tidak ditemukan.")

# Fungsi untuk mencari nomor plat pada data member
def cari_nomor_plat(plat_nomor):
    members = database.child("member").get()
    for member in members.each():
        data = member.val()
        if data["plat_nomor"] == plat_nomor:
            return True
    return False

# Fungsi untuk mengubah data parkir menjadi member jika ditemukan nomor plat yang sama
def ubah_ke_member(plat_nomor):
    if cari_nomor_plat(plat_nomor):
        database.child("parkir").child(plat_nomor).update({"member": True})
        print("Data parkir dengan plat nomor", plat_nomor, "telah diubah menjadi member.")
    else:
        print("Data member dengan plat nomor", plat_nomor, "tidak ditemukan.")

# Contoh penggunaan
tambah_data_parkir("AB123CD")
print("1")
tambah_data_parkir("EF456GH", member=True)  # Contoh tambahan dengan data member
print("2")
isi_jam_keluar("AB123CD", "17:30")
print("3")
cari_data_parkir("AB123CD")
print("4")
cari_data_parkir("EF456GH")
print("5")
ubah_ke_member("AB123CD")
print("6")
cari_data_parkir("AB123CD")
print("7")
cari_data_parkir("EF456GH")