import RPi.GPIO as GPIO
import time

# Inisialisasi pin Raspberry Pi untuk sensor ultrasonik dan servo
trigger_pin1 = 17
echo_pin1 = 18
trigger_pin2 = 23
echo_pin2 = 24
servo1_pin = 13
servo2_pin = 12

# Inisialisasi GPIO Raspberry Pi
GPIO.setmode(GPIO.BCM)
GPIO.setup(trigger_pin1, GPIO.OUT)
GPIO.setup(echo_pin1, GPIO.IN)
GPIO.setup(trigger_pin2, GPIO.OUT)
GPIO.setup(echo_pin2, GPIO.IN)
GPIO.setup(servo1_pin, GPIO.OUT)
GPIO.setup(servo2_pin, GPIO.OUT)

# Inisialisasi objek PWM (Pulse Width Modulation) untuk servo 1 dan servo 2
servo1_pwm = GPIO.PWM(servo1_pin, 50)  # Frekuensi PWM 50Hz
servo2_pwm = GPIO.PWM(servo2_pin, 50)  # Frekuensi PWM 50Hz

# Fungsi untuk mengukur jarak menggunakan sensor ultrasonik
def measure_distance(trigger_pin, echo_pin):
    GPIO.output(trigger_pin, GPIO.HIGH)
    time.sleep(0.00001)
    GPIO.output(trigger_pin, GPIO.LOW)

    pulse_start = time.time()
    pulse_end = time.time()

    while GPIO.input(echo_pin) == 0:
        pulse_start = time.time()

    while GPIO.input(echo_pin) == 1:
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start
    distance = pulse_duration * 17150
    distance = round(distance, 2)

    return distance

# Fungsi untuk menggerakkan servo 1 ke posisi tertentu
def move_servo1(position):
    duty_cycle = (position / 18.0) + 2.5  # Mengkonversi sudut menjadi siklus tugas (duty cycle)
    servo1_pwm.ChangeDutyCycle(duty_cycle)

# Fungsi untuk menggerakkan servo 2 ke posisi tertentu
def move_servo2(position):
    duty_cycle = (position / 18.0) + 2.5  # Mengkonversi sudut menjadi siklus tugas (duty cycle)
    servo2_pwm.ChangeDutyCycle(duty_cycle)

try:
    # Memulai PWM untuk servo 1 dan servo 2
    servo1_pwm.start(0)
    servo2_pwm.start(0)

    while True:
        # Mengukur jarak menggunakan sensor ultrasonik 1
        distance1 = measure_distance(trigger_pin1, echo_pin1)

        # Menggerakkan servo 1 berdasarkan jarak sensor ultrasonik 1
        if distance1 < 5:
            move_servo1(90)  # Menggerakkan servo 1 ke posisi 90 derajat
        else:
            move_servo1(0)  # Menggerakkan servo 1 ke posisi 0 derajat

        # Mengukur jarak menggunakan sensor ultrasonik 2
        distance2 = measure_distance(trigger_pin2, echo_pin2)

        # Menggerakkan servo 2 berdasarkan jarak sensor ultrasonik 2
        if distance2 < 5:
            move_servo2(90)  # Menggerakkan servo 2 ke posisi 90 derajat
        else:
            move_servo2(0)  # Menggerakkan servo 2 ke posisi 0 derajat

        time.sleep(0.1)  # Delay antara pembacaan sensor

except KeyboardInterrupt:
    # Memberhentikan PWM dan membersihkan pin GPIO saat program dihentikan
    servo1_pwm.stop()
    servo2_pwm.stop()
    GPIO.cleanup()
