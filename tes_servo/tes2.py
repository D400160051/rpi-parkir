import RPi.GPIO as GPIO
import time

# Inisialisasi pin Raspberry Pi untuk servo 1 dan servo 2
servo1_pin = 13
servo2_pin = 12

# Inisialisasi GPIO Raspberry Pi
GPIO.setmode(GPIO.BCM)
GPIO.setup(servo1_pin, GPIO.OUT)
GPIO.setup(servo2_pin, GPIO.OUT)

# Inisialisasi objek PWM (Pulse Width Modulation) untuk servo 1 dan servo 2
servo1_pwm = GPIO.PWM(servo1_pin, 50)  # Frekuensi PWM 50Hz
servo2_pwm = GPIO.PWM(servo2_pin, 50)  # Frekuensi PWM 50Hz

# Fungsi untuk menggerakkan servo 1 ke posisi tertentu
def move_servo1(position):
    duty_cycle = (position / 18.0) + 2.5  # Mengkonversi sudut menjadi siklus tugas (duty cycle)
    servo1_pwm.ChangeDutyCycle(duty_cycle)
    time.sleep(0.5)  # Delay 0.5 detik setelah perubahan posisi servo

# Fungsi untuk menggerakkan servo 2 ke posisi tertentu
def move_servo2(position):
    duty_cycle = (position / 18.0) + 2.5  # Mengkonversi sudut menjadi siklus tugas (duty cycle)
    servo2_pwm.ChangeDutyCycle(duty_cycle)
    time.sleep(0.5)  # Delay 0.5 detik setelah perubahan posisi servo

try:
    # Memulai PWM untuk servo 1 dan servo 2
    servo1_pwm.start(0)
    servo2_pwm.start(0)

    while True:
        # Menerima input dari pengguna
        input_key = input("Masukkan tombol (1, 2, 9, 0): ")

        # Menggerakkan servo berdasarkan input tombol
        if input_key == '1':
            move_servo1(10)  # Menggerakkan servo 1 ke posisi 0 derajat
        elif input_key == '2':
            move_servo1(100)  # Menggerakkan servo 1 ke posisi 90 derajat
        elif input_key == '9':
            move_servo2(10)  # Menggerakkan servo 2 ke posisi 0 derajat
        elif input_key == '0':
            move_servo2(100)  # Menggerakkan servo 2 ke posisi 90 derajat

        time.sleep(0.1)

except KeyboardInterrupt:
    # Mematikan PWM dan membersihkan GPIO saat program dihentikan
    servo1_pwm.stop()
    servo2_pwm.stop()
    GPIO.cleanup()
