import RPi.GPIO as GPIO
import time

# Inisialisasi pin Raspberry Pi untuk sensor ultrasonik
trigger_pin1 = 17
echo_pin1 = 18
trigger_pin2 = 23
echo_pin2 = 24

# Inisialisasi GPIO Raspberry Pi
GPIO.setmode(GPIO.BCM)
GPIO.setup(trigger_pin1, GPIO.OUT)
GPIO.setup(echo_pin1, GPIO.IN)
GPIO.setup(trigger_pin2, GPIO.OUT)
GPIO.setup(echo_pin2, GPIO.IN)

def measure_distance(trigger_pin, echo_pin):
    # Mengirim sinyal trigger
    GPIO.output(trigger_pin, GPIO.HIGH)
    time.sleep(0.00001)
    GPIO.output(trigger_pin, GPIO.LOW)

    pulse_start = time.time()
    pulse_end = time.time()

    # Menerima sinyal echo
    while GPIO.input(echo_pin) == 0:
        pulse_start = time.time()

    while GPIO.input(echo_pin) == 1:
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start

    # Menghitung jarak berdasarkan durasi sinyal
    distance = pulse_duration * 17150
    distance = round(distance, 2)

    return distance

try:
    while True:
        # Mengukur jarak menggunakan sensor ultrasonik 1
        distance1 = measure_distance(trigger_pin1, echo_pin1)
        print("Jarak Sensor 1:", distance1, "cm")
        time.sleep(0.2)  # Delay antara pembacaan sensor
        # Mengukur jarak menggunakan sensor ultrasonik 2
        distance2 = measure_distance(trigger_pin2, echo_pin2)
        print("Jarak Sensor 2:", distance2, "cm")

        time.sleep(0.2)  # Delay antara pembacaan sensor

except KeyboardInterrupt:
    GPIO.cleanup()
