import cv2
import numpy as np
import os

# Fungsi untuk melatih pembacaan plat nomor
def latih_pembaca_plat_nomor():
    # Inisialisasi variabel untuk menyimpan citra dan label
    data_training = []
    labels = []

    # Menginisialisasi kamera Raspberry Pi
    video_capture = cv2.VideoCapture(0)

    while True:
        # Membaca setiap frame dari kamera
        ret, frame = video_capture.read()

        # Menampilkan frame kamera
        cv2.imshow("Ambil Foto", frame)

        # Mengambil input dari keyboard
        key = cv2.waitKey(1)

        # Jika tombol 'c' ditekan, ambil foto dan tambahkan ke data latihan
        if key == ord('c'):
            # Mengubah citra menjadi citra grayscale
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Mengubah ukuran citra menjadi 200x50 piksel
            img = cv2.resize(gray, (200, 50))

            # Mengubah citra menjadi array numpy
            data = np.array(img, dtype=np.float32)

            # Menyimpan citra dan label ke dalam list
            data_training.append(data)
            labels.append(ord('A') - 65)  # Menggunakan label 'A' untuk foto dari kamera

            print("Mengambil foto...")

        # Jika tombol 'q' ditekan, keluar dari loop
        if key == ord('q'):
            break

    # Melepaskan sumber daya kamera
    video_capture.release()
    cv2.destroyAllWindows()

    # Mengubah list menjadi array numpy
    data_training = np.array(data_training)
    labels = np.array(labels)

    # Membuat model SVM untuk melatih data
    model = cv2.ml.SVM_create()
    model.setType(cv2.ml.SVM_C_SVC)
    model.setKernel(cv2.ml.SVM_LINEAR)
    model.train(data_training, cv2.ml.ROW_SAMPLE, labels)

    return model

# Fungsi untuk mendeteksi plat nomor menggunakan model yang telah dilatih
def deteksi_plat_nomor(img, model):
    # Mengubah ukuran citra menjadi 200x50 piksel
    img = cv2.resize(img, (200, 50))

    # Mengubah citra menjadi array numpy
    data = np.array(img, dtype=np.float32)

    # Menyesuaikan dimensi array untuk memenuhi kebutuhan model
    data = np.reshape(data, (1, -1))

    # Memprediksi label plat nomor
    _, result = model.predict(data)

    # Mengembalikan label dalam bentuk karakter
    return chr(result + 65)

# Contoh penggunaan
if __name__ == "__main__":
    # Melatih model pembacaan plat nomor dari kamera Raspberry Pi
    model = latih_pembaca_plat_nomor()

    # Membaca citra dari kamera Raspberry Pi
    video_capture = cv2.VideoCapture(0)

    while True:
        # Membaca setiap frame dari kamera
        ret, frame = video_capture.read()

        # Deteksi plat nomor pada setiap frame
        plat_nomor = deteksi_plat_nomor(frame, model)

        # Menampilkan plat nomor yang terdeteksi pada layar
        cv2.putText(frame, plat_nomor, (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        cv2.imshow("Deteksi Plat Nomor", frame)

        # Tombol 'q' untuk keluar dari program
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Melepas sumber daya kamera dan menutup jendela
    video_capture.release()
    cv2.destroyAllWindows()
