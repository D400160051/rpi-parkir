def compare_strings(str1, str2):
    # Fungsi untuk membandingkan dua string dan mengembalikan persentase kemiripan
    len1 = len(str1)
    len2 = len(str2)
    match_count = 0

    # Menghitung jumlah karakter yang cocok pada posisi yang sama
    for i in range(min(len1, len2)):
        if str1[i] == str2[i]:
            match_count += 1

    # Mengembalikan persentase kemiripan
    similarity = (match_count / max(len1, len2)) * 100
    return similarity

def check_similarity(input_var, database_file):
    # Membaca database dari file teks
    with open(database_file, 'r') as file:
        database = file.read().splitlines()

    # Memeriksa kemiripan variabel input dengan setiap entri dalam database
    for entry in database:
        similarity = compare_strings(input_var, entry)
        if similarity >= 50:
            print(f"Variabel '{input_var}' memiliki kemiripan {similarity}% dengan '{entry}' dalam database.")
            return

    # Jika tidak ada kesamaan ditemukan
    print(f"Tidak ada kesamaan dengan entri dalam database untuk variabel '{input_var}'.")

# Contoh penggunaan
input_var = input("Masukkan variabel: ")
database_file = "database.txt"  # Ganti dengan lokasi dan nama file database Anda

check_similarity(input_var, database_file)
