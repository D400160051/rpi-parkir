import cv2
import imutils as im
import numpy as np     #Library OpenCV
import pytesseract   
# Fungsi untuk membaca kontur dari video kamera
def read_contours_from_camera():
    # Membuka kamera
    video = cv2.VideoCapture(0)

    while True:
        # Membaca frame dari kamera
        ret, frame = video.read()

        # Konversi frame menjadi grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        blur = cv2.bilateralFilter(gray, 8, 75, 75)

        # Melakukan thresholding pada frame grayscale
        thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 3)

        # Mencari kontur dalam frame threshold
        imgcnt = thresh.copy()

        cnt = cv2.findContours(imgcnt, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnt = im.grab_contours(cnt)
        cnt = sorted(cnt, key=cv2.contourArea, reverse=True)[:10]

        detected = None
        screencnt = None

        for c in cnt:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)

            # Jika kontur memiliki 4 sudut (aproximasi empat sisi)
            if len(approx) == 4:
                screencnt = approx
                detected = True
                break

        # Jika kontur terdeteksi
        if detected:
            # Menggambar kontur pada frame
            cv2.drawContours(frame, [screencnt], -1, (0, 255, 0), 2)

            # Masking bagian lain
            mask = np.zeros(thresh.shape, np.uint8)
            new_image = cv2.drawContours(mask, [screencnt], 0, 255, -2)
            new_image = cv2.bitwise_and(frame, frame, mask=mask)

            # Proses pemotongan (cropping)
            (x, y) = np.where(mask == 255)
            (topx, topy) = (np.min(x), np.min(y))
            (bottomx, bottomy) = (np.max(x), np.max(y))
            cropped = gray[topx:bottomx + 1, topy:bottomy + 1]

            blur = cv2.GaussianBlur(cropped, (5, 5), 0)
            ret, th = cv2.threshold(blur, 127, 255, cv2.THRESH_BINARY_INV)
            text = pytesseract.image_to_string(th, config='--oem 3 --psm 6 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
            print("Detected Number is:", text)

        # Menampilkan frame yang telah digambar kontur
        cv2.imshow('Camera Contour', frame)

        # Tombol 'q' untuk keluar dari loop
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Menutup kamera dan jendela tampilan
    video.release()
    cv2.destroyAllWindows()

# Memanggil fungsi untuk membaca kontur dari kamera
read_contours_from_camera()
