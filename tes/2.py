import pyrebase
import RPi.GPIO as GPIO
import time

# Inisialisasi GPIO untuk buzzer
buzzer_pin = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(buzzer_pin, GPIO.OUT)

# Konfigurasi Firebase
config = {
    "apiKey": "AIzaSyCc5YgY8mZSENPlE7pdTr9FgrCDVhBxrSM",
    "authDomain": "eparkingdempo.firebaseapp.com",
    "databaseURL": "https://eparkingdempo-default-rtdb.asia-southeast1.firebasedatabase.app",
    "storageBucket": "eparkingdempo.appspot.com"
}

# Inisialisasi Pyrebase
firebase = pyrebase.initialize_app(config)

# Mendapatkan referensi database
db = firebase.database()

# Mendapatkan nilai yang ingin dicari
nilai_dicari = "nilai yang ingin dicari"

# Mengecek apakah terdapat child dengan nilai yang dicari
result = db.child("parkir").order_by_value().equal_to(nilai_dicari).get()

if result.each():
    print("Child ditemukan!")
    # Lanjutkan eksekusi yang diinginkan
else:
    print("Child tidak ditemukan!")
    # Menghidupkan buzzer
    GPIO.output(buzzer_pin, GPIO.HIGH)
    time.sleep(1)  # Menghidupkan buzzer selama 1 detik
    GPIO.output(buzzer_pin, GPIO.LOW)

# Membersihkan GPIO
GPIO.cleanup()
