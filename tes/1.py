import cv2

def segmentasi_plat_nomor(gambar):
    # Mengubah gambar ke dalam skala abu-abu
    gambar_abu = cv2.cvtColor(gambar, cv2.COLOR_BGR2GRAY)

    # Menerapkan thresholding adaptif untuk memisahkan teks dari latar belakang
    _, threshold = cv2.threshold(gambar_abu, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Menemukan kontur pada gambar hasil thresholding
    kontur, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Menginisialisasi daftar segmen teks
    segmen_teks = []

    # Mengiterasi melalui setiap kontur
    for c in kontur:
        # Mendapatkan koordinat dan ukuran bounding box kontur
        x, y, w, h = cv2.boundingRect(c)

        # Menentukan batasan untuk memfilter segmen teks yang tidak relevan berdasarkan ukuran
        min_width, min_height = 30, 80
        max_width, max_height = 200, 150

        # Memfilter segmen teks yang memenuhi batasan ukuran
        if min_width <= w <= max_width and min_height <= h <= max_height:
            # Memotong dan menyimpan segmen teks
            segmen = gambar[y:y+h, x:x+w]
            segmen_teks.append(segmen)

    # Mengembalikan daftar segmen teks
    return segmen_teks
# Membuat objek VideoCapture untuk membaca gambar dari kamera (indeks kamera 0)
kamera = cv2.VideoCapture(0)

while True:
    # Membaca gambar dari kamera
    ret, frame = kamera.read()

    # Melakukan segmentasi plat nomor pada gambar
    segmen_teks = segmentasi_plat_nomor(frame)

    # Menampilkan hasil segmentasi
    for i, segmen in enumerate(segmen_teks):
        cv2.imshow('Segmen {}'.format(i+1), segmen)

    # Tombol keyboard untuk keluar dari loop
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # Menunggu tombol keyboard ditekan untuk menangkap gambar plat nomor
    if cv2.waitKey(1) & 0xFF == ord('c'):
        # Menyimpan gambar plat nomor sebagai file
        cv2.imwrite('plat_nomor.png', frame)
        print("Gambar plat nomor telah disimpan!")

# Menghentikan pengambilan gambar dan menutup jendela
kamera.release()
cv2.destroyAllWindows()