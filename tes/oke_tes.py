import cv2             #library OpenCV
import imutils as im   #Library ImageUtility
import numpy as np     #Library OpenCV
import pytesseract     #lybrary OCR
from collections import Counter #library DEEPLEARNING
import time             #biar bisa delay

# Daftar nomor plat yang terdeteksi
detected_plates = []
camera = cv2.VideoCapture(0)
def baca_plat():
    

    # Ambil foto
    print("ambil foto")
    return_value, image = camera.read()

    # Simpan foto pada direktori tertentu
    cv2.imwrite('/home/pi/rpi-parkir/foto.jpg', image)
    print("simpan foto")
    # Bebaskan kamera
    #del(camera)


    img = cv2.imread('/home/pi/rpi-parkir/foto.jpg')
    img = cv2.resize(img,(620,480))

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('/home/pi/rpi-parkir/gray.jpg', gray)
    #blur = cv2.GaussianBlur(gray,(5,5),0)
    #blur = cv2.medianBlur(gray,5)
    blur = cv2.bilateralFilter(gray, 9, 75, 75)
    cv2.imwrite('/home/pi/rpi-parkir/blur.jpg', blur)

    th = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    #edge = cv2.Canny(blur,50,150)
    cv2.imwrite('/home/pi/rpi-parkir/th.jpg', th)
    imgcnt = th.copy()

    cnt = cv2.findContours(imgcnt,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cnt = im.grab_contours(cnt)
    cnt = sorted(cnt,key=cv2.contourArea,reverse = True)[:10]

    
    detected = None
    screencnt = None
    for c in cnt:
        
        peri = cv2.arcLength(c,True)
        approx = cv2.approxPolyDP(c,0.02*peri,True)
        #if there are four DP 
        if len(approx) == 4:
            screencnt = approx
                #detected == 1
            break
            
    #masking other part
    mask = np.zeros(gray.shape,np.uint8)
    cv2.imwrite('/home/pi/rpi-parkir/mask.jpg', mask)

    try:
        new_image = cv2.drawContours(mask,[screencnt],0,255,-2)#mengapa menggunakan -1
        new_image = cv2.bitwise_and(img,img,mask=mask)
        #cropping process
        (x,y) = np.where(mask ==255)
        (topx,topy) = (np.min(x),np.min(y))
        (bottomx,bottomy) = (np.max(x),np.max(y))
        cropped = gray[topx:bottomx+1, topy:bottomy+1]
        cv2.imwrite('/home/pi/rpi-parkir/cp.jpg', cropped)
        blur = cv2.GaussianBlur(cropped,(5,5),-2)
        ret,th = cv2.threshold(blur,127,255,cv2.THRESH_BINARY_INV)
        cv2.imwrite('/home/pi/rpi-parkir/th.jpg', th)
        text = pytesseract.image_to_string(th,config='--oem 3 --psm 6 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
        print("Detected Number is:",text)
        newline_index = text.find("\n")

        # Memilih hanya bagian atas string
        top_part = text[:newline_index]

        # Menghapus spasi di bagian atas string
        top_part = top_part.replace(" ", "")

        # Cek apakah variabel kosong
        if top_part:
            detected_plates.append(top_part)
                
        # Hitung jumlah kemunculan setiap nomor plat
        counts = Counter(detected_plates)
               
        # Ambil nomor plat dengan kemunculan terbanyak
        most_common_plate = counts.most_common(1)[0][0]
                
        # Cetak nomor plat yang paling sering muncul
        print("Nomor Plat Terdeteksi:", most_common_plate)
    except:
        print('no detected contour')

while True:

    print('loop start')
    baca_plat()
    print('loop stop')
    time.sleep(1)
# baca_plat()
#cv2.imshow('frame','/home/pi/rpi-parkir/foto.jpg')
cv2.waitKey(0)
cv2.destroyAllWindows()

#hello
