from collections import Counter

# Daftar nomor plat yang terdeteksi
detected_plates = []

try:
    while True:
        # Minta input nomor plat dari pengguna
        plate_number = input("Masukkan nomor plat: ")
        
        # Tambahkan nomor plat ke daftar terdeteksi
        detected_plates.append(plate_number)
        
        # Hitung jumlah kemunculan setiap nomor plat
        counts = Counter(detected_plates)
        
        # Ambil nomor plat dengan kemunculan terbanyak
        most_common_plate = counts.most_common(1)[0][0]
        
        # Cetak nomor plat yang paling sering muncul
        print("Nomor Plat Terdeteksi:", most_common_plate)
        
except KeyboardInterrupt:
    pass
