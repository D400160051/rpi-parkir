#------------------------------------------------------------header ---------------------------------------
import RPi.GPIO as GPIO #library GPIO
import pyrebase        #library firebase  
import cv2             #library OpenCV
import imutils as im   #Library ImageUtility
import numpy as np     #Library OpenCV
import pytesseract     #lybrary OCR(optic caracter recognoition)
from collections import Counter #library collection
import time             #library waktu bisa delay
from datetime import datetime, timedelta  #library tanggal dan waktu 
import os               #lybrary OS akses system 
import qrcode           #lybrary untuk membuat QRCODE
import re               #lybrary 
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from lib_tft24T import TFT24T
import spidev           
from cek import check_similarity
#------------------------------------------------------------Variable ---------------------------------------
lama_gerbang = 5
plat_parkir = " "
lock = " "
qrdir = "/home/pi/rpi-parkir/TEMP_FOTO/qr.jpg"
database_file = "/home/pi/rpi-parkir/DB.txt"
tankyou = "/home/pi/rpi-parkir/TEMP_FOTO/smile.jpg"
biaya_parkir_per_jam = 1000  # Harga per jam parkir
biaya_parkir_minimal = 1000  # Biaya parkir minimal jika kurang dari 1 jam
on = True
hitung = 0
percobaan_gagal = 60

# Variabel global untuk melacak posisi saat ini
current_position_servo1 = 0
current_position_servo2 = 0
#------------------------------------------------------------inisialisasi ---------------------------------------

# Inisialisasi pin Raspberry Pi untuk sensor ultrasonik dan servo

trigger_pin1 = 17
echo_pin1 = 18
trigger_pin2 = 23
echo_pin2 = 24
servo1_pin = 13 #gerbang masuk 
servo2_pin = 12 #gerbang keluar 
buzzer_pin = 21 #peringatan
DC = 5
RST = 6

#inisialisasi nomor kamera masuk dan keluar 
kamera_masuk = 0
kamera_keluar = 2

# Inisialisasi GPIO Raspberry Pi
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(trigger_pin1, GPIO.OUT)
GPIO.setup(echo_pin1, GPIO.IN)
GPIO.setup(trigger_pin2, GPIO.OUT)
GPIO.setup(echo_pin2, GPIO.IN)
GPIO.setup(servo1_pin, GPIO.OUT)
GPIO.setup(servo2_pin, GPIO.OUT)
GPIO.setup(buzzer_pin, GPIO.OUT)

# Inisialisasi objek PWM (Pulse Width Modulation) untuk servo 1 dan servo 2
servo1_pwm = GPIO.PWM(servo1_pin, 50)  # Frekuensi PWM 50Hz
servo2_pwm = GPIO.PWM(servo2_pin, 50)  # Frekuensi PWM 50Hz

servo1_pwm.start(0)
servo2_pwm.start(0)

# Create TFT LCD/TOUCH object:
TFT = TFT24T(spidev.SpiDev(), GPIO, landscape=False)

# Initialize display.
TFT.initLCD(DC, RST)
# If rst is omitted then tie rst pin to +3.3V
# If led is omitted then tie led pin to +3.3V

# Get the PIL Draw object to start drawing on the display buffer.
draw = TFT.draw()

def lcd_tampil(jpg):
    #TFT.clear((255, 0, 0))
    # Alternatively can clear to a black screen by simply calling:
    TFT.clear()
    print("Loading image...")
    image = Image.open(jpg)
    # Resize the image and rotate it so it's 240x320 pixels.
    image = image.rotate(90,0,1).resize((240, 320))
    # Draw the image on the display hardware.
    print("Drawing image")
    TFT.display(image)

def lcd_tes(text2):
    TFT.clear((90,90,255))
    font = ImageFont.truetype('FreeSerifItalic.ttf', 40)
    draw.textrotated((100,10), text2 ,90 ,font=font, fill="RED")
    TFT.display()
lcd_tes("lcd-tes")

#cek koneksi camera
def check_camera_detection(hw):
    cap = cv2.VideoCapture(hw)
    if cap.isOpened():
        print("Kamera terdeteksi USB : ", hw )
        time.sleep(1)
        cap.release()
    else:
        print("Kamera tidak terdeteksi")     
           
lcd_tes("tes-kamera")
check_camera_detection(kamera_masuk)  #camera0 
check_camera_detection(kamera_keluar)  #camera2 

#cek koneksi internet
print("cek koneksi internet")


#setting API Firebase
config = {
    "apiKey": "AIzaSyCc5YgY8mZSENPlE7pdTr9FgrCDVhBxrSM",
    "authDomain": "eparkingdempo.firebaseapp.com",
    "databaseURL": "https://eparkingdempo-default-rtdb.asia-southeast1.firebasedatabase.app",
    "storageBucket": "eparkingdempo.appspot.com"
}
firebase = pyrebase.initialize_app(config)
database = firebase.database()

#cek database awal koneksi 
print("cek database")
lcd_tes("cek database")
data = database.get().val()
# print("cek: " + str(data))
print("database connected")
lcd_tampil(tankyou)

#-----------------------------------------------------------fungsi-fungsi---------------------------------------

#fungsi buzzer dalam satuan detik 
def beep(duration):
    GPIO.output(21, GPIO.HIGH)  # Turn on beep
    print("beep")
    time.sleep(duration)        # Wait for the specified duration
    GPIO.output(21, GPIO.LOW)   # Turn off beep

# Baca suhu CPU dari file sistem thermal_zone0
def get_cpu_temperature():
    with open("/sys/class/thermal/thermal_zone0/temp", "r") as f:
        temp = float(f.read()) / 1000.0
    return temp

# Fungsi untuk mengukur jarak menggunakan sensor ultrasonik
def measure_distance(trigger_pin, echo_pin):
    GPIO.output(trigger_pin, GPIO.HIGH)
    time.sleep(0.00001)
    GPIO.output(trigger_pin, GPIO.LOW)

    pulse_start = time.time()
    pulse_end = time.time()

    while GPIO.input(echo_pin) == 0:
        pulse_start = time.time()

    while GPIO.input(echo_pin) == 1:
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start
    distance = pulse_duration * 17150
    distance = round(distance, 2)

    return distance

# Fungsi untuk menggerakkan servo ke posisi tertentu dengan perlahan
def gerbang_masuk(position):
    global current_position_servo1

    steps = int(abs(position - current_position_servo1))  # Jumlah langkah untuk mencapai posisi tujuan
    direction = 1 if position > current_position_servo1 else -1  # Arah gerakan servo (1: maju, -1: mundur)

    for _ in range(steps):
        current_position_servo1 += direction
        duty_cycle = (current_position_servo1 / 18.0) + 2.5  # Mengkonversi sudut menjadi siklus tugas (duty cycle)
        servo1_pwm.ChangeDutyCycle(duty_cycle)
        time.sleep(0.01)  # Jeda waktu antara setiap langkah

    print("gerbang masuk")

# Fungsi untuk menggerakkan servo ke posisi tertentu dengan perlahan
def gerbang_keluar(position):
    global current_position_servo2

    steps = int(abs(position - current_position_servo2))  # Jumlah langkah untuk mencapai posisi tujuan
    direction = 1 if position > current_position_servo2 else -1  # Arah gerakan servo (1: maju, -1: mundur)

    for _ in range(steps):
        current_position_servo2 += direction
        duty_cycle = (current_position_servo2 / 18.0) + 2.5  # Mengkonversi sudut menjadi siklus tugas (duty cycle)
        servo2_pwm.ChangeDutyCycle(duty_cycle)
        time.sleep(0.01)  # Jeda waktu antara setiap langkah

    print("gerbang keluar")

def isi_kondisi(plat_nomor,kondisi):
    database.child(plat_nomor).get().val()
    database.child(plat_nomor).update({"sudahMasuk": kondisi})
    print("Kondisi berhasil diisi.")

# Fungsi untuk menambahkan data parkir/member
def tambah_data_parkir(plat_nomor):
    tanggal = datetime.now().strftime("%d-%m-%Y")
    jam_masuk = datetime.now().strftime("%H:%M")
    waktu_masuk = f"{tanggal} {jam_masuk}"  # Menggabungkan tanggal dan jam masuk
    waktu_sekarang = time.time()
    print("Waktu saat ini (Epoch):", waktu_sekarang)
    data = {                                    # PrimariKey (plat nomor kendaraan) RW +baca primari key untuk add data +jika plat nomor tidak ditemukan buat database baru+
            "email": "",                        # String                            W  +jika plat nomor tidak ditemukan buat database baru+
            "langganan": {
            "lamaLangganan": 0,                 # Number                            W  +jika plat nomor tidak ditemukan buat database baru+
            "sudahLangganan": False,            # Bolean                            RW +jika plat nomor tidak ditemukan buat database baru+
            "waktuBerhentiLangganan": "",       # String (Format dd-mm-yyyy HH:MM)  RW +mengecek apakah langganan masih valid +jika plat nomor tidak ditemukan buat database baru+
            "waktuMulaiLangganan": ""           # String (Format dd-mm-yyyy HH:MM)  W  +jika plat nomor tidak ditemukan buat database baru+
            },
            "nama": "",                         # String                            W  +jika plat nomor tidak ditemukan buat database baru+
            "nomor_handphone": "",              # Number                            W  +jika plat nomor tidak ditemukan buat database baru+
            "riwayat": {
            int(waktu_sekarang): {              # String (epoch/unix time NTP)      W  +jika plat nomor tidak ditemukan buat database baru+
                "bayarParkir": 0,               # Number                            W  +ditulis saat terdeteksi akan keluar parkir +jika plat nomor tidak ditemukan buat database baru+
                "waktuKeluar": "",              # String (Format dd-mm-yyyy) ?      W  +ditulis saat terdeteksi akan keluar parkir +jika plat nomor tidak ditemukan buat database baru+
                "waktuMasuk": waktu_masuk       # String (Format dd-mm-yyyy HH:MM)  W  +ditulis saat terdeteksi masuk parkir +jika plat nomor tidak ditemukan buat database baru+
                }
            },
            "saldo": 0,                         # Number (write 0 initial)          W  +jika plat nomor tidak ditemukan buat database baru+
            "sudahMasuk":True                   # bolean                           RW +mendeteksi kondisi kendaraan +jika plat nomor tidak ditemukan buat database baru+
                
            }
    database.child(plat_nomor).set(data)
    print("ADD database")

def notif(isi):
    tanggal = datetime.now().strftime("%d-%m-%Y")
    jam = datetime.now().strftime("%H:%M:%S")
    waktu_sekarang = time.time()  
    waktu_eror = f"{tanggal} {jam}"  # Menggabungkan tanggal dan jam 
    data_riwayat = {
        "waktuEror": waktu_eror,
        "Pesan": isi
    }
    database.child("notification").child(int(waktu_sekarang)).set(data_riwayat)
    print("notif add")

def tambah_riwayat(plat_nomor):
    tanggal = datetime.now().strftime("%d-%m-%Y")
    jam_masuk = datetime.now().strftime("%H:%M")
    waktu_masuk = f"{tanggal} {jam_masuk}"  # Menggabungkan tanggal dan jam masuk
    waktu_sekarang = time.time()
    print("Waktu saat ini (Epoch):", waktu_sekarang)
    data_riwayat = {
        "waktuMasuk": waktu_masuk,
        "waktuKeluar": "",
        "bayarParkir": 0
    }
    database.child(plat_nomor).child("riwayat").child(int(waktu_sekarang)).set(data_riwayat)
    database.child(plat_nomor).update({"sudahMasuk": True})
    print("Menambahkan riwayat baru")

def cari_platnomor(var_input):
    data = database.get().val()
    for key in data.keys():
        if key == var_input:
            print("Parent ada!")
            return True
    print("Parent tidak ditemukan.")
    return False

def isi_jam_keluar(plat_nomor):
    tanggal = datetime.now().strftime("%d-%m-%Y")
    jam_keluar = datetime.now().strftime("%H:%M")
    waktu_keluar = f"{tanggal} {jam_keluar}"  # Menggabungkan tanggal dan jam masuk
    waktu_sekarang = time.time()
    # Mendapatkan data riwayat
    riwayat = database.child(plat_nomor).child("riwayat").get().val()
    if riwayat:
        # Mendapatkan ID riwayat dengan waktu paling baru (terakhir)
        id_riwayat_terakhir = max(riwayat.keys(), key=int)

        # Mengupdate waktu keluar dalam database
        database.child(plat_nomor).child("riwayat").child(id_riwayat_terakhir).update({"waktuKeluar": waktu_keluar})

        print("Waktu keluar pada entri riwayat terakhir berhasil diisi.")
        return True
    else:
        print("Plat nomor tidak ditemukan dalam riwayat.")
        return False

def isi_pembayaran(plat_nomor,bayar):
    riwayat = database.child(plat_nomor).child("riwayat").get().val()

    if riwayat:
        # Mendapatkan ID riwayat dengan waktu paling baru (terakhir)
        id_riwayat_terakhir = max(riwayat.keys(), key=int)

        # Mengupdate waktu keluar dalam database
        database.child(plat_nomor).child("riwayat").child(id_riwayat_terakhir).update({"bayarParkir": bayar})

        print("pembayaran di tulis.")
        return True
    else:
        print("gagal tulis.")

def deteksi_plat_nomor(plat_nomor):
    
    pola = r"[A-Z]{1,3}\d{4}[A-Z]{2,3}$"
    hasil = re.match(pola, plat_nomor)
    if hasil:
        return True
    else:
        return False

# Fungsi untuk menghasilkan QR Code
def generate_qr_code(data, filename):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    img.save(filename)

def convert_epoch(waktu_keluar):
    format_waktu = "%d-%m-%Y %H:%M"  # Format waktu keluar (dd/mm/yy HH:MM)
    waktu_obj = datetime.strptime(waktu_keluar, format_waktu)
    waktu_epoch = int(waktu_obj.timestamp())
    return waktu_epoch    

#funngsi baca plat nomor menggunakan no camera
def baca_plat(hwc):
    global plat_parkir
    global lock
    global baca_kamera
    plat_parkir = ""
    # Daftar nomor plat yang terdeteksi
    detected_plates = []
    camera = cv2.VideoCapture(hwc)
    mm = False  # Inisialisasi kondisi sebagai False
    while not mm:
        return_value, image = camera.read()
        # Pengecekan apakah kamera siap
        if return_value:
        # Simpan foto pada direktori tertentu
            cv2.imwrite("/home/pi/rpi-parkir/TEMP_FOTO/foto.jpg", image)
            print("Foto berhasil disimpan.")

            mm = True
        else:
            print("Kamera tidak siap atau tidak dapat membaca gambar.")
            del(camera)
            camera = cv2.VideoCapture(hwc)
    #most_common_plate = ""
    cam = True
    top_part = ""
    counting  = 0
    while cam:
        # Membaca frame dari kamera
        ret, frame = camera.read()

        # Konversi frame menjadi grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        blur = cv2.bilateralFilter(gray, 9, 75, 75)
        # Melakukan thresholding pada frame grayscale
        thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 3)
        # Mencari kontur dalam frame threshold
        imgcnt = thresh.copy()

        cnt = cv2.findContours(imgcnt, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnt = im.grab_contours(cnt)
        cnt = sorted(cnt, key=cv2.contourArea, reverse=True)[:10]

        detected = None
        screencnt = None

        for c in cnt:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.05 * peri, True)

            # Jika kontur memiliki 4 sudut (aproximasi empat sisi)
            if len(approx) == 4:
                screencnt = approx
                detected = True
                break

        # Jika kontur terdeteksi
        if detected:
            # Menggambar kontur pada frame
            cv2.drawContours(frame, [screencnt], -1, (0, 255, 0), 2)

            # Masking bagian lain
            mask = np.zeros(thresh.shape, np.uint8)
            new_image = cv2.drawContours(mask, [screencnt], 0, 255, -2)
            new_image = cv2.bitwise_and(frame, frame, mask=mask)

            # Proses pemotongan (cropping)
            (x, y) = np.where(mask == 255)
            (topx, topy) = (np.min(x), np.min(y))
            (bottomx, bottomy) = (np.max(x), np.max(y))
            cropped = gray[topx:bottomx + 1, topy:bottomy + 1]
            cv2.imwrite('/home/pi/rpi-parkir/TEMP_FOTO/cp.jpg', cropped)
            blur = cv2.GaussianBlur(cropped, (5, 5), 0)
            ret, th = cv2.threshold(blur, 127, 255, cv2.THRESH_BINARY_INV)
            text = pytesseract.image_to_string(th, config='--oem 3 --psm 6 -c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
            print("Detected:", text)
           # Memecah teks menjadi baris-baris
            lines = text.splitlines()

            # Menghapus spasi dari setiap baris
            lines_without_spaces = [line.replace(" ", "") for line in lines]

            # Mencari baris terpanjang berdasarkan panjangnya
            longest_line = max(lines_without_spaces, key=len)
            print("Nomor Plat Terdeteksi:", longest_line)
        # Bebaskan kamera
        result = check_similarity(longest_line, database_file)
        if deteksi_plat_nomor(result):
            plat_parkir = result
            print("Nomor Plat Terdeteksi:", plat_parkir)
            baca_kamera = plat_parkir
            del(camera)
            lock = 1
            cam = False
        else:
            print(result, "bukan plat nomor Indonesia yang valid.")
            lock = 0
            beep(0.001)
            counting = counting + 1
            print(counting)
            if counting == percobaan_gagal:
                print("terlalu banyak gagal ")
                cam = False
                

#------------------------------------------------------------ loop program ---------------------------------------
while True:
    
    print("loop start \n")
    now = datetime.now()
    print("JAM", now.strftime("%d-%m-%Y %H:%M:%S"))
     # Ambil suhu CPU saat ini
    cpu_temp = get_cpu_temperature()
    # Tampilkan suhu CPU
    print("Suhu CPU: {:.2f} ".format(cpu_temp))

    # Mengukur jarak menggunakan sensor ultrasonik 
    distance1 = measure_distance(trigger_pin1, echo_pin1)
    distance2 = measure_distance(trigger_pin2, echo_pin2)
    print("Sensor Masuk:", distance1, "cm")
    print("Sensor Keluar:", distance2, "cm")
    #============================= gerbang masuk ================================
    if distance1 < 6:
        print("gerbang masuk \n")
        beep(0.1)
        baca_plat(kamera_masuk)
        if lock == 1:
            if cari_platnomor(plat_parkir):
                tambah_riwayat(plat_parkir)
            else :
                tambah_data_parkir(plat_parkir)
            gerbang_masuk(110)
            time.sleep(lama_gerbang)
            gerbang_masuk(0)
        else :
            notif("Pembacaan gagal terlalu banyak di gerbang masuk")
            beep(5) 

    #============================= gerbang keluar ================================
    elif distance2 < 6:
        on = True
        print("gerbang keluar \n")
        beep(0.2)
        baca_plat(kamera_keluar)
        if lock == 1:
            if cari_platnomor(plat_parkir) :
                kondisi = database.child(plat_parkir).child("sudahMasuk").get().val()
                if kondisi==True:
                    print("Kondisi True \n")
                    isi_jam_keluar(plat_parkir)
                    riwayat = database.child(plat_parkir).child("riwayat").get().val()
                    if riwayat:
                        id_riwayat_terakhir = max(riwayat.keys(), key=int)
                        riwayat_terakhir = riwayat[id_riwayat_terakhir]
                        waktu_keluar = riwayat_terakhir.get("waktuKeluar")
                        waktu_masuk  = riwayat_terakhir.get("waktuMasuk")
                        waktu_keluar1    = convert_epoch(waktu_keluar)
                        waktu_masuk1     = convert_epoch(waktu_masuk)
                        selisih_waktu = int(waktu_keluar1)- int(waktu_masuk1)
                        print(selisih_waktu)

                    member = database.child(plat_parkir).child("langganan").child("sudahLangganan").get().val()

                    if selisih_waktu < 3600:                            #3600 mewakili 1 jam dalam detik 
                        biaya_parkir = biaya_parkir_minimal             #biaya parkir minimal 
                    else:
                        jam_parkir = selisih_waktu // 3600                  #perhitungan biaya parkir bila melebihi 1 jam  dalam detik 
                        menit_parkir = (selisih_waktu % 3600) // 60         #perhitungan kelebihan waktu 
                        biaya_parkir = jam_parkir * biaya_parkir_per_jam    #perhitungan biaya parkir
                        if menit_parkir > 0:
                            biaya_parkir += biaya_parkir_per_jam            #jika ada lebih maka akan di tambah / dibulatkan ke 1 jam

                    print("Biaya Parkir:", biaya_parkir, "rupiah")
                    isi_pembayaran(plat_parkir,biaya_parkir)

                    if member==True:
                        print("gerbang keluar member \n")
                        isi_kondisi(plat_parkir,False)
                        gerbang_keluar(110)
                        time.sleep(lama_gerbang)
                        gerbang_keluar(0)
                    else : 
                        print("gerbang keluar non-member \n")
                        generate_qr_code(biaya_parkir, qrdir)
                        lcd_tampil(qrdir)
                        while on:
                            result = database.child(plat_parkir).child("sudahMasuk").get().val()
                            print("Menunggu pembayaran")
                            print(result)
                            time.sleep(1.5)
                            hitung = hitung+1
                            print(hitung)
                            if result == False :
                                on = False
                                print("Sudah dibayar OK ")
                                lcd_tampil(tankyou)

                        gerbang_keluar(110)
                        time.sleep(lama_gerbang)
                        gerbang_keluar(0)
                else :
                    print("Kondisi False tidak memenuhi syarat \n")
                    notif(str(plat_parkir)+" Kondisi Belum pernah masuk")
                    beep(2)
#-------------------------------------------------
            else :
                print("Plat nomor tidak ditemukan \n")
                notif(str(plat_parkir)+" Plat nomor tidak ditemukan di database")
                beep(2)
        else:
            notif("Pembacaan gagal terlalu banyak di gerbang keluar")
            beep(5) 
    print("")

    print("loop stop\n")
    hitung = hitung+1
    print(hitung)
    time.sleep(0.5) #delay baca sensor 
    servo1_pwm.start(0)
    servo2_pwm.start(0)
    if hitung == 99999:
        hitung = 0
# #------------------------------------------------------------loop program ---------------------------------------

#end
